
-- avoid memory leak
collectgarbage("setpause", 100) 
collectgarbage("setstepmul", 5000)

-- run
cc.FileUtils:getInstance():addSearchPath("scripts")
-- CC_USE_DEPRECATED_API = true
require "cocos.init"


-- cclog
cclog = function(...)
    print(string.format(...))
end

-- for CCLuaEngine traceback
function __G__TRACKBACK__(msg)
    cclog("----------------------------------------")
    cclog("LUA ERROR: " .. tostring(msg) .. "\n")
    cclog(debug.traceback())
    cclog("----------------------------------------")
    return msg
end

----------------


local director = cc.Director:getInstance()
local glView   = director:getOpenGLView()
if nil == glView then
    glView = cc.GLViewImpl:createWithRect("game", cc.rect(0,0,640,900))
    director:setOpenGLView(glView)
end

--turn on display FPS
director:setDisplayStats(true)

--set FPS. the default value is 1.0/60 if you don't call this
director:setAnimationInterval(1.0 / 60)

-- local screenSize = glView:getFrameSize()

-- local designSize = {width = 320, height = 480}


-- glView:setDesignResolutionSize(designSize.width, designSize.height, cc.ResolutionPolicy.FIXED_HEIGHT)

--support debug
local targetPlatform = cc.Application:getInstance():getTargetPlatform()
if (cc.PLATFORM_OS_IPHONE == targetPlatform) or (cc.PLATFORM_OS_IPAD == targetPlatform) or
	(cc.PLATFORM_OS_ANDROID == targetPlatform) or (cc.PLATFORM_OS_WINDOWS == targetPlatform) or
	(cc.PLATFORM_OS_MAC == targetPlatform) then
	cclog("result is ")
     --require('debugger')()
end

local function myadd(x, y)
    return x + y
end
cclog("result is " .. myadd(1, 1))


-- require "mainMenu"

-- local scene = cc.Scene:create()
-- scene:addChild(CreateTestMenu())
-- if cc.Director:getInstance():getRunningScene() then
--     cc.Director:getInstance():replaceScene(scene)
-- else
--     cc.Director:getInstance():runWithScene(scene)
-- end

xpcall(main, __G__TRACKBACK__)