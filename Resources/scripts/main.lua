require "visibleRect"

function onCreateMenuScene(scene)
	scene:setName("menuScene")
end

function onCreateMenuLayer(layer)
	layer:setName("menuLayer")

	local visibleSize = cc.Director:getInstance():getVisibleSize()
	local origin = cc.Director:getInstance():getVisibleOrigin()

	label = cc.Label:createWithTTF("Stack Master", "fonts/Marker Felt.ttf", 54)

 	--position the label on the center of the screen
 	label:setPosition(cc.p(origin.x + visibleSize.width/2, origin.y + visibleSize.height - label:getContentSize().height)) 	
	layer:addChild(label)

	-- sprite = cc.Sprite:create("HelloWorld.png")
	-- sprite:setPosition(cc.p(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y))
	-- layer:addChild(sprite)

end

function onCreateGameScene(scene)
	scene:setName("GameScene")
	-- scene:getPhysicsWorld():setDebugDrawMask(cc.PhysicsWorld.DEBUGDRAW_ALL)

    

	-- fade = cc.TransitionFade:create(3, scene)
    -- cc.Director:getInstance():replaceScene(fade)
end

function onCreateGameLayer(layer)
	layer:setName("GameLayer")
	-- local origin = cc.Director:getInstance():getVisibleOrigin()
	-- sprite = cc.Sprite:create("HelloWorld.png")
	-- sprite:setPosition(cc.p(VisibleRect:getVisibleRect().width/2 + origin.x, VisibleRect:getVisibleRect().height/2 + origin.y))
	-- layer:addChild(sprite)
end

function onCreateBoardNode(node)
	node:setName("Board")
	-- node:setPhysicsBody(cc.PhysicsBody:createEdgeBox(cc.size(VisibleRect:getVisibleRect().width-160, VisibleRect:getVisibleRect().height-200)))
    node:setPosition(VisibleRect:center())
 --    node:getPhysicsBody():setDynamic(false)

 --    cc.Director:getInstance():getRunningScene():getPhysicsWorld()
   
end

function onCreateBoardPhysicsBody(body)
	local numHorizontalBlock = 20
	local numVerticalBlock = 17
	local blockSize = 24
	local thickness = 32
	-- shapeBoxBottom = cc.PhysicsShapeBox:create(cc.size(blockSize*numHorizontalBlock, thickness), cc.PHYSICSBODY_MATERIAL_DEFAULT, 
	-- 	cc.p(0, -(blockSize*numVerticalBlock)/2))
	-- body:addShape(shapeBoxBottom)
	
	-- shapeBoxLeft = cc.PhysicsShapeBox:create(cc.size(thickness,blockSize*numVerticalBlock+thickness), cc.PHYSICSBODY_MATERIAL_DEFAULT, 
	-- 	cc.p(-(blockSize*numHorizontalBlock+thickness)/2, 0))
	-- body:addShape(shapeBoxLeft)
	-- shapeBoxRight = cc.PhysicsShapeBox:create(cc.size(thickness,blockSize*numVerticalBlock+thickness), cc.PHYSICSBODY_MATERIAL_DEFAULT, 
	-- 	cc.p((blockSize*numHorizontalBlock+thickness)/2, 0))
	-- body:addShape(shapeBoxRight)
end
