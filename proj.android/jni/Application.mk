APP_STL := c++_static
NDK_TOOLCHAIN_VERSION=clang

APP_CPPFLAGS := -frtti -DCC_ENABLE_CHIPMUNK_INTEGRATION=1 -std=c++11 -fsigned-char
APP_LDFLAGS := -latomic


ifeq ($(NDK_DEBUG),1)
  APP_CPPFLAGS += -DCOCOS2D_DEBUG=1
  APP_OPTIM := debug
#  APP_CFLAGS := -O3
else
  APP_CPPFLAGS += -DNDEBUG
  APP_OPTIM := release
  APP_CFLAGS := -O3
endif

# ./build_native.py -p 10   -b debug   -n APP_ABI=armeabi-v7a
# adb -d install -r bin/game-debug.apk
# ndk-gdb.py  --force --verbose -d
# b jni/../../Classes/GameScene.cpp:
# b jni/../../cocos2d/cocos/./physics/CCTaskScheduler.cpp
# b /Users/fspadoni/Projects/chipmunk2D/src/cpSpaceSoftBody2.c: