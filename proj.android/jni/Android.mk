LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/external)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/cocos)

LOCAL_MODULE := cocos2dcpp_shared

LOCAL_MODULE_FILENAME := libcocos2dcpp

LOCAL_SRC_FILES := hellocpp/main.cpp \
                   ../../Classes/AppDelegate.cpp \
                   ../../Classes/HelloWorldScene.cpp \
                   ../../Classes/Main.cpp \
                   ../../Classes/GameScene.cpp \
                   ../../Classes/GameLayer.cpp \
                   ../../Classes/Block.cpp \
                   ../../Classes/BlockStatus.cpp \
                   ../../Classes/BlockFactory.cpp \
                   ../../Classes/Board.cpp \
                   ../../Classes/LevelSelectionScene.cpp \
                   ../../Classes/LevelLoader.cpp \
                   ../../Classes/GameConfig.cpp \
                   ../../Classes/VisibleRect.cpp 

LOCAL_C_INCLUDES := $(LOCAL_PATH)/../../Classes

LOCAL_STATIC_LIBRARIES := cocos2dx_static
LOCAL_STATIC_LIBRARIES := cocos2d_lua_static

include $(BUILD_SHARED_LIBRARY)

$(call import-module,scripting/lua-bindings/proj.android)
$(call import-module,.)