//
//  Board.cpp
//  game
//
//  Created by Federico Spadoni on 05/11/14.
//
//

#include "Board.h"

#include "GameLayer.h"
#include "VisibleRect.h"
#include "lua_helper_funcs.h"
#include "physics/CCPhysicsBody.h"

USING_NS_CC;

bool Board::init()
{
    Node::init();
 
    callLuaFunction<Node>("scripts/main.lua","onCreateBoardNode", static_cast<Node*>(this), "cc.Node");
    
    PhysicsBody* body = PhysicsBody::create();
    body->setDynamic(false);
    
    const int width = 1024;
	const int thickness = 32;
    
    
    auto shapeBoxBottom = PhysicsShapeBox::create(Size(width, thickness), PhysicsMaterial(0.0f, 0.5f, 0.5f), Size(0.0, - VisibleRect::getVisibleRect().size.height/2 + thickness) );
    
    shapeBoxBottom->setCategoryBitmask( CollisionCategory::platform );
    shapeBoxBottom->setCollisionBitmask( ~CollisionCategory::platform );
    
    body->addShape(shapeBoxBottom);
    
    callLuaFunction<PhysicsBody>("scripts/main.lua","onCreateBoardPhysicsBody", static_cast<PhysicsBody*>(body), "cc.PhysicsBody");
    
//    body->addShape(PhysicsShapeBox::create(size, material, border, offset));
//    body->addShape(PhysicsShapeBox::create(size, material, border, offset));
//    body->addShape(PhysicsShapeBox::create(size, material, border, offset));
    
//    body->autorelease();

    this->setPhysicsBody(body);
    
//    setPhysicsBody(PhysicsBody::createEdgeBox(VisibleRect::getVisibleRect().size));
//    setPosition(VisibleRect::center());
//    getPhysicsBody()->setDynamic(false);
    
    
    return true;
}