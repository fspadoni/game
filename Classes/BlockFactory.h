//
//  BlockFactory.h
//  game
//
//  Created by Federico Spadoni on 08/11/14.
//
//

#ifndef game_BlockFactory_h
#define game_BlockFactory_h

#include "base/CCRef.h"
#include "base/CCVector.h"
#include "physics-utils/CCMemoryBuffer.h"
#include "physics/CCMaterialProperty.h"
//#include "physics/CCPageMemoryPool.h"

#include <map>

NS_CC_BEGIN
class TriangularMesh;
//class PhysicsSoftBody;
NS_CC_END

class Block;


class BlockFactory : public cocos2d::Ref
{
  
    
public:
    
    static BlockFactory* getInstance();
    
    static void destroyInstance();

    void reset();
    
    bool isInitialized();
    
    bool addElementFromFile(const std::string &filename );
    bool addMaterialFromFile(const std::string &filename);
    
    Block* getBlock();
    
    inline size_t getNumMaterials() {return _materials.size();}
    
    inline const cocos2d::Vector<cocos2d::SoftBodyMaterial*>& getMaterials() {return _materials;}
    
    inline void setScaling(const float scale) {_scaleBlock = scale;}
    
CC_CONSTRUCTOR_ACCESS:
    BlockFactory();
    
    virtual ~BlockFactory();
    
private:
    
    static BlockFactory* s_singleInstance;
    
//    cocos2d::PageMemoryPool* memPool;
    
    // memory buffer to initialize the PageMemoryPool
//    cocos2d::BufferSharedPtr<char> memoryBuffer;
    
    
    cocos2d::Vector<cocos2d::TriangularMesh*> _triMeshes;
    cocos2d::Vector<cocos2d::SoftBodyMaterial*> _materials;
    
    std::map<cocos2d::SoftBodyMaterial*, cocos2d::Vector<Block*> > _softBodiesMap;
    
    typedef std::map<cocos2d::SoftBodyMaterial*, cocos2d::Vector<Block*> >::iterator _softBodiesMapIterator;
    typedef std::map<cocos2d::SoftBodyMaterial*, cocos2d::Vector<Block*> >::const_iterator _softBodiesMapConstIterator;
    
    float _scaleBlock;
};

#endif
