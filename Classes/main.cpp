//
//  main.cpp
//  game
//
//  Created by Federico Spadoni on 03/11/14.
//
//

#include "main.h"

#include "lua_helper_funcs.h"
#include "GameScene.h"
#include "BlockFactory.h"
#include "LevelLoader.h"
#include "physics-utils/CCPageMemoryPool.h"

USING_NS_CC;


Scene* Main::createScene()
{

    auto scene = Scene::create();
    callLuaFunction<Scene>("scripts/main.lua","onCreateMenuScene", scene, "cc.Scene");
    
//    // 'layer' is an autorelease object
    auto layer = Main::create();
    callLuaFunction<Layer>("scripts/main.lua","onCreateMenuLayer", layer, "cc.Layer");
    
//    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool Main::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.
    
    // create menu, it's an autorelease object
    auto menu = Menu::create();
    // add a "close" icon to exit the progress. it's an autorelease object
    auto closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(Main::menuCloseCallback, this));
    closeItem->setScale( (visibleSize.height / 8) / closeItem->getContentSize().height );
	closeItem->setPosition(Vec2(origin.x + visibleSize.width/2 - closeItem->getContentSize().width/2 ,
                                origin.y + 2*closeItem->getContentSize().height));
    
    menu->addChild(closeItem);
    
    // add menu items for tests
    auto playLabel = Label::createWithTTF("Play", "fonts/Marker Felt.ttf", 48);
    auto playItem = MenuItemLabel::create( playLabel, CC_CALLBACK_1(Main::menuPlayCallback, this));
    playItem->setPosition(Vec2( visibleSize.width/2 + origin.x,  visibleSize.height/2 + origin.y));
    menu->addChild(playItem);

//    // add menu items for tests
//    auto exitLabel = Label::createWithTTF("Exit", "fonts/Marker Felt.ttf", 48);
//    auto exitItem = MenuItemLabel::create( exitLabel, CC_CALLBACK_1(Main::menuCloseCallback, this));
//    exitItem->setPosition(Vec2( visibleSize.width/2 + origin.x,  visibleSize.height/2 + origin.y - 2*exitItem->getContentSize().height));
//    menu->addChild(exitItem);
    
    
//    this->addChild(playLabel);
    
    
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);

    if ( !BlockFactory::getInstance()->isInitialized() )
    {
        BlockFactory::getInstance()->setScaling(16.0);
        BlockFactory::getInstance()->addElementFromFile("meshes/I/block_I");
        BlockFactory::getInstance()->addElementFromFile("meshes/O/block_O");
        BlockFactory::getInstance()->addElementFromFile("meshes/T/block_T");
        BlockFactory::getInstance()->addElementFromFile("meshes/S/block_S");
        BlockFactory::getInstance()->addElementFromFile("meshes/Z/block_Z");
        BlockFactory::getInstance()->addElementFromFile("meshes/L/block_L");
        BlockFactory::getInstance()->addElementFromFile("meshes/J/block_J");
        
        BlockFactory::getInstance()->addMaterialFromFile("materials/soft.xml");
        BlockFactory::getInstance()->addMaterialFromFile("materials/hard.xml");
        BlockFactory::getInstance()->addMaterialFromFile("materials/plastic.xml");
    }
//    LevelLoader::getInstance()->addFile();
    
    
    return true;
}

void Main::onEnter()
{
    Layer::onEnter();
}

void Main::onExit()
{

    Layer::onExit();
}

void Main::cleanup()
{
    Layer::cleanup();
}


void Main::menuPlayCallback(Ref* pSender)
{
    const char* levelName = LevelLoader::getFirstLevel() ;
    LevelLoader* loader = LevelLoader::load( levelName );
    
    // create the game scene and run it
    auto scene = GameScene::create(loader);

    
    if (scene)
    {
//        auto fade = TransitionFade::create(3, scene)
        Director::getInstance()->replaceScene(scene);
//        scene->release();
    }

    
}

void Main::menuCloseCallback(Ref* pSender)
{
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.","Alert");
    return;
#endif
    
    BlockFactory::getInstance()->destroyInstance();
    
    Director::getInstance()->end();
    
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

//const LevelLoader* Main::loadFirstLevel()
//{
//    
//    if (const char* levelName = LevelLoader::getFirstLevel() )
//    {
//        if (const LevelLoader* loader = LevelLoader::load(levelName) )
//        {
//            return loader;
//        }
//    }
//    
//    CCLOG("first level not found");
//    return nullptr;
//}
