//
//  GameLayer.cpp
//  game
//
//  Created by Federico Spadoni on 10/04/15.
//
//

#include "GameLayer.h"
#include "GameScene.h"
#include "Block.h"
#include "BlockFactory.h"
#include "Board.h"
#include "GameConfig.h"
#include "LevelLoader.h"
#include "VisibleRect.h"

#include "physics-render/CCSoftBodyDrawNode.h"

#include "chipmunk.h"


USING_NS_CC;

const float PI_2 = 2.0 * 3.1415926535897932385f;



GameLayer* GameLayer::create(const LevelLoader* loader)
{
    GameLayer *ret = new (std::nothrow) GameLayer(loader);
    if (ret && ret->init())
    {
        ret->autorelease();
        return ret;
    }
    else
    {
        CC_SAFE_DELETE(ret);
        return nullptr;
    }
}



GameLayer::GameLayer(const LevelLoader* loader)
: _loader(loader)
, _scene(nullptr)
, _physicsWorld(nullptr)
, _currentBlock(nullptr)
, _keyTranslate(false)
, _keyRotate(false)
, _delta(Vec2::ZERO)
, _scoreLabel(nullptr)
, _scoreIcon(nullptr)
, _blockFragmentCounter(0)
, _blockBrokenCounter(0)
{
    _blockBrokenCounter = GameConfig::getInstance()->_maxBrokenBlocks;
    
}

GameLayer::~GameLayer()
{
    _currentBlock->release();
}


bool GameLayer::init()
{
    Layer::init();
    
    //    auto blockFactory = BlockFactory::getInstance();
    
    Node::scheduleUpdate();
    
    
    //    create draw node for softbodies
    const cocos2d::Vector<cocos2d::SoftBodyMaterial*>& materials = BlockFactory::getInstance()->getMaterials();
    
    cocos2d::Vector<cocos2d::SoftBodyMaterial*>::const_iterator last = materials.end();
    for (cocos2d::Vector<cocos2d::SoftBodyMaterial*>::const_iterator iter = materials.begin(); iter != last; ++iter )
    {
        auto drawNode = SoftBodyDrawNode::create( (*iter)->getTextureFileName() );
        addSoftBodyDrawNode( drawNode, (*iter) );
        //        drawNode->setPosition(VisibleRect::top());
        this->addChild(drawNode);
    }
    
    
    initBoard();
    
    bool initialized = initLevel();
    
    return initialized;
}


void GameLayer::initBoard()
{
    PhysicsBody* body = PhysicsBody::create();
    body->setDynamic(false);
    
    const int width = 1024;
	const int thickness = 32;
    
    auto shapeBoxBottom = PhysicsShapeBox::create(Size(width, thickness), PhysicsMaterial(0.0f, 0.5f, 0.5f) );
    //    VisibleRect::Size(0.0, - VisibleRect::getVisibleRect().size.height/2 + thickness)
    
    shapeBoxBottom->setCategoryBitmask( CollisionCategory::platform );
    shapeBoxBottom->setCollisionBitmask( ~CollisionCategory::platform );
    
    body->addShape(shapeBoxBottom);
    
    //create a sprite
    auto board = Sprite::create("textures/cromecemtwlk.jpg");
    board->setPosition( VisibleRect::bottom().x, thickness );
    float scaleX = VisibleRect::getVisibleRect().size.width / board->getContentSize().width;
    float scaleY = (thickness + 1) / board->getContentSize().height;
    board->setScale(scaleX, scaleY );
    //apply physicsBody to the sprite
//    board->setPhysicsBody(body);
    board->addComponent(body);
    
    this->addChild(board);
}




bool GameLayer::initLevel()
{
    
    const Vector<LevelBlock*>& levelBlocks = _loader->getLevelBlocks();
    
    Vector<LevelBlock*>::const_iterator lastIter = levelBlocks.cend();
    
    for (Vector<LevelBlock*>::const_iterator iter = levelBlocks.cbegin(); iter != lastIter; ++iter )
    {
        
        TriangularMesh* mesh = (*iter)->getMesh();
        SoftBodyMaterial* material = (*iter)->getMaterial();
        
        //        check if the material is already loaded
        SoftBodyDrawNode* drawNode = nullptr;
        const SoftBodyDrawNodesIterator drawNodeLastIter = _softbodyDrawNodes.end();
        for ( SoftBodyDrawNodesIterator drawNodeIter = _softbodyDrawNodes.begin(); drawNodeIter != drawNodeLastIter;  ++drawNodeIter )
        {
            //            be carefull to have different name for each material!
            if ( material->getName() == drawNodeIter->first->getName()
                || material->getTextureFileName() == drawNodeIter->first->getTextureFileName() )
            {
                material = drawNodeIter->first;
                drawNode = drawNodeIter->second;
                break;
            }
        }
        
        const Vec2 pos = (*iter)->getPosition();
        
        Block* levelBlock = Block::createFromTriangularMesh(mesh, material);
        _levelLoadedBlocks.pushBack(levelBlock);
        
        levelBlock->setPosition( (*iter)->getPosition() );
        levelBlock->setLevelBlock(true);
        
        //        set filter for constrained nodes
        if ( const unsigned int numConstrainedNodes = levelBlock->getNumConstrainedNodes() )
        {
            const unsigned short* constrNodes = levelBlock->getConstrainedNodes();
            
            for (int i=0; i<numConstrainedNodes; ++i)
            {
                levelBlock->setPointCollisionFilter(*constrNodes++, CollisionCategory::platform, ~CollisionCategory::platform);
            }
            
        }
        
        
        if ( drawNode )
        {
            drawNode->addSoftBody(levelBlock);
        }
        else
        {
            drawNode = SoftBodyDrawNode::create( (*iter)->getMaterial()->getTextureFileName() );
            addSoftBodyDrawNode( drawNode, (*iter)->getMaterial() );
            drawNode->addSoftBody(levelBlock);
            
            this->addChild(drawNode);
        }
        
        
        
    }
    
    
    return true;
}

void GameLayer::onEnter()
{
    Layer::onEnter();
    
    _scene = dynamic_cast<cocos2d::PhysicsScene*>(this->getParent());
    
    //    add blocks loaded from level to the scene
    Vector<Block*>::const_iterator lastIter = _levelLoadedBlocks.cend();
    
    
    for (Vector<Block*>::const_iterator iter = _levelLoadedBlocks.cbegin(); iter != lastIter; ++iter )
    {
        _scene->addSoftBodyToPhysicsWorld(*iter);
    }
    
    
    //    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    _currentBlock = BlockFactory::getInstance()->getBlock();
    _currentBlock->retain();
    //    _currentBlock->autorelease();
    
    _currentBlock->setLayer(this);
    
    SoftBodyMaterial* material = _currentBlock->getMaterials();
    
    SoftBodyDrawNodesIterator drawNodeIter = _softbodyDrawNodes.find(material);
    SoftBodyDrawNode* drawNode = drawNodeIter->second;
    drawNode->addSoftBody(_currentBlock);
    
    _currentBlock->setPosition( VisibleRect::top() );
    _currentBlock->setVelocity( GameConfig::getInstance()->_fallVelocity);
    
    
    //    currentBlock->setSleepCallback( std::bind(&GameLayer::pushNextBlock, this, std::placeholders::_1) );
    //    _currentBlock->setBreakAllCallback( std::bind(&SoftBodyDrawNode::removeSoftBody, drawNode, std::placeholders::_1) );
    _currentBlock->notifyBreakElement(true);
    _currentBlock->notifyBreak(true);
    _currentBlock->notifyBeginCollision(true);
    _currentBlock->notifySeparateCollision(true);
    
    _currentBlock->setCollisionFilter(CollisionCategory::currentBlock, CP_ALL_CATEGORIES);
    
    schedule(CC_SCHEDULE_SELECTOR(GameLayer::pushNextBlock), 4.0, CC_REPEAT_FOREVER, 4.0);
    
    _scene->addSoftBodyToPhysicsWorld(_currentBlock);
    
    //    reset gravity
    _currentBlock->setGravity(0.0,0.0);
    
    
    // Register Touch Event
    //    setTouchEnabled(true);
    //    setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
    auto listener = EventListenerTouchOneByOne::create();
    listener->setSwallowTouches(true);
    
    listener->onTouchBegan = CC_CALLBACK_2(GameLayer::onTouchBegan, this);
    listener->onTouchMoved = CC_CALLBACK_2(GameLayer::onTouchMoved, this);
    listener->onTouchEnded = CC_CALLBACK_2(GameLayer::onTouchEnded, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    _touchListener = listener;
    
    // Register Keyboard Event
    //    setKeyboardEnabled(true);
    _keyboardListener = EventListenerKeyboard::create();
    _keyboardListener->onKeyPressed = CC_CALLBACK_2(GameLayer::onKeyPressed, this);
    _keyboardListener->onKeyReleased = CC_CALLBACK_2(GameLayer::onKeyReleased, this);
    
    _eventDispatcher->addEventListenerWithSceneGraphPriority(_keyboardListener, this);
    
    
    // score icones
    Size visibleSize = Director::getInstance()->getVisibleSize();
    
    for (int i=0; i<GameConfig::getInstance()->_maxBrokenBlocks; ++i)
    {
        _brokenBlockSprites[i] = Sprite::create("textures/red.png");
        _brokenBlockSprites[i]->setScale(visibleSize.height * 0.04 / _brokenBlockSprites[i]->getContentSize().height );
        _brokenBlockSprites[i]->setPosition( VisibleRect::rightTop().x - visibleSize.height * 0.004 * _brokenBlockSprites[i]->getContentSize().width - i * visibleSize.height * 0.006 * _brokenBlockSprites[i]->getContentSize().width,
                                VisibleRect::rightTop().y -  visibleSize.height * 0.002 * _brokenBlockSprites[i]->getContentSize().height );
        
        this->addChild(_brokenBlockSprites[i]);
    }
    
    
    _scoreIcon = Sprite::create("textures/blue.png");
    _scoreIcon->setScale(visibleSize.height * 0.04 / _scoreIcon->getContentSize().height);
    _scoreIcon->setPosition( VisibleRect::rightTop().x -  visibleSize.height * 0.004 *_scoreIcon->getContentSize().width,
                            VisibleRect::rightTop().y -  visibleSize.height * 0.009 *_scoreIcon->getContentSize().height );
    
//    scoreIcon->setPosition( scoreLabel->getPosition().x, scoreLabel->getPosition().y + scoreIcon->getContentSize().height );
    //    scoreIcon->setColor(Color3B::RED);
    this->addChild(_scoreIcon);
    
    _scoreLabel = Label::createWithTTF("0", "fonts/Marker Felt.ttf", 20);
    _scoreLabel->setScale(visibleSize.height * 0.04 / _scoreLabel->getContentSize().height);
    _scoreLabel->setPosition( _scoreIcon->getPosition().x - _scoreLabel->getContentSize().width -_scoreIcon->getContentSize().width, _scoreIcon->getPosition().y );
//    scoreLabel->setColor(Color3B::GREEN);
    this->addChild(_scoreLabel);
    
    
//    Director::getInstance()->Director::resume();
//    this->resume();
    _scene->simulationResume();
}

void GameLayer::onExit()
{
    
    Layer::onExit();
}

void GameLayer::cleanup()
{
    
    Layer::cleanup();
}


void GameLayer::update(float delta)
{
    
    Node::update(delta);
    
}

void GameLayer::updateThreadSafe(float delta)
{
    
    static const float frequency = 1.0 / Director::getInstance()->getAnimationInterval();
    
    
//    if ( !_currentBlock->isStacked() )
    if ( _currentBlock->getStatus() == &Block::Active )
    {
        _currentBlock->setVelocity(0.0, 0.0);
        
        if (_keyRotate)
        {
            _currentBlock->setAngularVelocity(_delta.y*frequency);
        }
        
        if ( _keyTranslate )
        {
            _currentBlock->addVelocity(_delta.x*frequency, 0.0 );
        }

        
        if ( !_keyRotate && !_keyTranslate )
        {
            _currentBlock->addVelocity( GameConfig::getInstance()->_fallVelocity );
        }
        
    }
    else if ( _currentBlock->getStatus() == &Block::Falling )
    {
        _currentBlock->setVelocity( GameConfig::getInstance()->_fallVelocity );
    }
    else if ( _currentBlock->getStatus() == &Block::Stacked )
    {
        
        //    check if block reaches the top goal
        Rect topGoal( VisibleRect::center().x - GameConfig::getInstance()->_topGoalWidth/2,
                     (GameConfig::getInstance()->_topGoalHeight - 8 ),
                     GameConfig::getInstance()->_topGoalWidth, 16.0);
        
        _scene->getPhysicsWorld()->queryRect( CC_CALLBACK_3(GameLayer::checkForLevelCompletion, this), topGoal, CollisionCategory::platform, ~CollisionCategory::platform, nullptr);
        
    }
    
    
    
    ThreadSafeCallbacksIter lastIter = _threadSafeCallbacks.end();
    for (ThreadSafeCallbacksIter iter = _threadSafeCallbacks.begin(); iter != lastIter; ++iter)
    {
        if ( (*iter)() )
        {
            _threadSafeCallbacks.clear();
            return;
        }
    }
    
    _threadSafeCallbacks.clear();

}


void GameLayer::pushNextBlock(float dt)
{
    _threadSafeCallbacks.push_back( CC_CALLBACK_0(GameLayer::doPushNextBlock, this) );
}

void GameLayer::increaseBlockFragmentCounter()
{
    ++_blockFragmentCounter;
    _threadSafeCallbacks.push_back( CC_CALLBACK_0(GameLayer::doIncreaseBlockFragmentCounter, this) );
}

void GameLayer::increaseBlockBrokenCounter()
{
    
    _threadSafeCallbacks.push_back( CC_CALLBACK_0(GameLayer::doIncreaseBrokenCounter, this) );
   
    if ( --_blockBrokenCounter == 0 )
    {
        _threadSafeCallbacks.push_back( CC_CALLBACK_0(GameLayer::doStackFailed, this) );
    }
}

bool GameLayer::doIncreaseBlockFragmentCounter()
{
    char buffer[8];
    sprintf(buffer, "%d", _blockFragmentCounter );
    _scoreLabel->setString(buffer);
    
    return false;
}

bool GameLayer::doIncreaseBrokenCounter()
{

    this->removeChild(_brokenBlockSprites[_blockBrokenCounter]);
    
    return false;
}

bool GameLayer::doStackFailed()
{
    auto failLevel = Label::createWithTTF("Stack Failed", "fonts/Marker Felt.ttf", 48);
    failLevel->setPosition(VisibleRect::center());
    this->addChild(failLevel);
    
    _scene->simulationPause();
//    this->pause();
    
    return true;
}

bool GameLayer::doPushNextBlock()
{
    
    _currentBlock->setCollisionFilter(CollisionCategory::block, CP_ALL_CATEGORIES);
    if (!_currentBlock->isStacked() )
    {
        _currentBlock->setVelocity(GameConfig::getInstance()->_fallVelocity);
        _currentBlock->setGravity( _scene->getPhysicsWorld()->getGravity() );
    }
    _currentBlock->release();
    
    _currentBlock = BlockFactory::getInstance()->getBlock();
    _currentBlock->retain();
    
    _currentBlock->setLayer(this);
    
    _currentBlock->setPosition( VisibleRect::top() );
    _currentBlock->setVelocity(GameConfig::getInstance()->_fallVelocity);
    
    SoftBodyMaterial* material = _currentBlock->getMaterials();
    SoftBodyDrawNodesIterator drawNodeIter = _softbodyDrawNodes.find(material);
    SoftBodyDrawNode* drawNode = drawNodeIter->second;
    drawNode->addSoftBody(_currentBlock);
    
    
    //    currentBlock->setSleepCallback( std::bind(&GameLayer::pushNextBlock, this, std::placeholders::_1) );
    //    _currentBlock->setBreakAllCallback( std::bind(&SoftBodyDrawNode::removeSoftBody, drawNode, std::placeholders::_1) );
    _currentBlock->notifyBreakElement(true);
    _currentBlock->notifyBreak(true);
    _currentBlock->notifyBeginCollision(true);
    _currentBlock->notifySeparateCollision(true);
    
    _currentBlock->setCollisionFilter(CollisionCategory::currentBlock, CP_ALL_CATEGORIES);
    
    _scene->addSoftBodyToPhysicsWorld(_currentBlock);
    
    _currentBlock->setGravity(0.0,0.0);
    _currentBlock->setVelocity(GameConfig::getInstance()->_fallVelocity);
    
    return false;
}

bool GameLayer::doTouchBegan()
{
//    _blockPosition = _currentBlock->getPosition();
//    _startTouchPoint = touch->getLocation();
    _keyTranslate = true;
    return false;
}


bool GameLayer::doTouchMoved()
{
    static const float k_rotation = PI_2 / (VisibleRect::getVisibleRect().size.width / 2 );
    static const float frequency = 1.0 / Director::getInstance()->getAnimationInterval();

    if ( _currentBlock->isStacked() )
        return false;
    
    const float dx = _delta.x;
    const float dy = _delta.y;
    
        
    if ( _currentBlock->isStacked() )
        return false;
    
    if ( dx > dy )
    {
        if ( dx > -dy )
        {
            // 1 moveRight
            _currentBlock->setVelocity(_delta.x*frequency, 0.0);
//            _keyTranslate = _keyRotate = true;
        }
        else
        {
            {
                // 2 rotate();
                _currentBlock->setAngularVelocity(_delta.y* k_rotation*frequency);
//                _keyRotate = _keyRotate = true;
            }
        }
    }
    else if ( dx < dy )
    {
        if ( -dx > dy )
        {
            // 4 moveLeft;
            _currentBlock->setVelocity(_delta.x*frequency, 0.0);
//            _keyTranslate = _keyRotate = true;
        }
        else
        {
            
            {
                // 5 rotate();
                _currentBlock->setAngularVelocity(_delta.y* k_rotation*frequency);
//                _keyRotate = _keyRotate = true;
            }
            
        }
    }
    
    return false;
}



bool GameLayer::doTouchEnded()
{
    _keyTranslate = false;
//    _keyRotate = false;
    return false;
}


bool GameLayer::onTouchBegan(Touch* touch, Event* event)
{
    
    _threadSafeCallbacks.push_back( CC_CALLBACK_0(GameLayer::doTouchBegan, this) );
    
    return true;
}


void GameLayer::onTouchMoved(Touch* touch, Event* event)
{

    _delta = touch->getDelta();

    _threadSafeCallbacks.push_back( CC_CALLBACK_0(GameLayer::doTouchMoved, this) );
}



void GameLayer::onTouchEnded(Touch* touch, Event* event)
{
    _keyTranslate = false;
    _keyRotate = false;
}

void GameLayer::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* unused_event)
{
    static const float dx = 4;
    static const float da = 2.0 * PI_2 / 120.0;
    
    if ( keyCode == EventKeyboard::KeyCode::KEY_LEFT_ARROW )
    {
        _keyTranslate = true;
        _delta.x = -dx;
    }
    if (keyCode == EventKeyboard::KeyCode::KEY_RIGHT_ARROW )
    {
        _keyTranslate = true;
        _delta.x = dx;
    }
    if ( keyCode == EventKeyboard::KeyCode::KEY_UP_ARROW )
    {
        _keyRotate = true;
        _delta.y = da;
    }
    if (keyCode == EventKeyboard::KeyCode::KEY_DOWN_ARROW )
    {
        _keyRotate = true;
        _delta.y = -da;
    }
}

void GameLayer::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* unused_event)
{
    _keyTranslate = false;
    _keyRotate = false;
}

bool GameLayer::checkForLevelCompletion(cocos2d::PhysicsWorld& world, cocos2d::PhysicsShape& shape, void* data)
{
    //    check if the stack is high enough .. to avoid explosion stack scompleted in firsts level
    
    Rect stackHeight( VisibleRect::center().x - GameConfig::getInstance()->_stackCheckGoalWidth/2,
                 (GameConfig::getInstance()->_stackCheckGoalHeight - 8 ),
                 GameConfig::getInstance()->_stackCheckGoalWidth, 16.0);
    
    
    _scene->getPhysicsWorld()->queryRect( CC_CALLBACK_3(GameLayer::levelCompleted, this), stackHeight, CollisionCategory::platform, ~CollisionCategory::platform, nullptr);
}

bool GameLayer::levelCompleted(cocos2d::PhysicsWorld& world, cocos2d::PhysicsShape& shape, void* data)
{
//    if ( Director::getInstance()->isPaused() )
//        return false;
    
    _threadSafeCallbacks.push_back( CC_CALLBACK_0(GameLayer::doLevelCompleted, this) );
    
    return true;
}

bool GameLayer::doLevelCompleted()
{
    auto completeLevel = Label::createWithTTF("Stack Complete", "fonts/Marker Felt.ttf", 48);
    completeLevel->setPosition(VisibleRect::center());
    this->addChild(completeLevel);
    
    _scene->simulationPause();
//    this->pause();
    
    return true;
    
}

void GameLayer::toggleDebugCallback(bool debugDraw)
{
    
    SoftBodyDrawNodesIterator last = _softbodyDrawNodes.end();
    for (SoftBodyDrawNodesIterator iter = _softbodyDrawNodes.begin(); iter != last; ++iter )
    {
        iter->second->setWireFrame(debugDraw);
    }
    
}


void GameLayer::onTouchMovedOld(Touch* touch, Event* event)
{
    static const float k_rotation = PI_2 / (VisibleRect::getVisibleRect().size.width / 2 );
    static const float frequency = 1.0 / Director::getInstance()->getAnimationInterval();
    Vec2 delta = touch->getDelta();
    const float dx = delta.x;
    const float dy = delta.y;
    
    if ( _currentBlock->isStacked() )
        return;
    
    //    if ( delta.lengthSquared() < 1 )
    //    {
    //        //        bad trick
    //        _keyTranslate = true;
    //        _delta.x = _delta.y = 0.0;
    //        return;
    //    }
    
    if ( dx > dy )
    {
        if ( dx > -dy )
        {
            // 1 moveRight
            //            _currentBlock->translate( delta.x, 0.0 );
            _currentBlock->setVelocity(delta.x*frequency, 0.0);
            //            _delta.x = dx;
            _keyTranslate = true;
        }
        else
        {
            //            if ( dx > 0 )
            {
                // 2 rotate();
                //                _currentBlock->rotate( delta.y * k_rotation);
                _currentBlock->setAngularVelocity(delta.y* k_rotation*frequency);
                //                _delta.y = delta.y * k_rotation;
                _keyRotate = true;
            }
            //            else
            //            {
            //                // 3 rotate();
            ////                _currentBlock->setAngularVelocity(-delta.y * k_rotation);
            //                _delta.y = - delta.y * k_rotation;
            //                _keyRotate = true;
            //
            //            }
        }
    }
    else if ( dx < dy )
    {
        if ( -dx > dy )
        {
            // 4 moveLeft;
            //            _currentBlock->translate( delta.x, 0.0);
            _currentBlock->setVelocity(delta.x*frequency, 0.0);
            _keyTranslate = true;
        }
        else
        {
            
            {
                // 5 rotate();
                _currentBlock->setAngularVelocity(delta.y* k_rotation*frequency);
                _keyRotate = true;
            }
            
        }
    }
}


void GameLayer::onTouchMovedOldOld(Touch* touch, Event* event)
{
    static const float k_rotation = PI_2 / (VisibleRect::getVisibleRect().size.width / 2 );
    static const float frequency = 1.0 / Director::getInstance()->getAnimationInterval();
    Vec2 delta = touch->getDelta();
    const float dx = delta.x;
    const float dy2 = delta.y * 2;
    
    if ( delta.lengthSquared() < 2 )
    {
        //        bad trick
        _keyTranslate = true;
        _delta.x = _delta.y = 0.0;
        return;
    }
    
    if ( dx > dy2 )
    {
        if ( dx > -dy2 )
        {
            // 1 moveRight
            //            _currentBlock->translate( delta.x, 0.0 );
            _currentBlock->setVelocity(delta.x*frequency, 0.0);
            //            _delta.x = dx;
            _keyTranslate = true;
        }
        else
        {
            if ( dx > 0 )
            {
                // 2 rotate();
                //                _currentBlock->rotate( delta.y * k_rotation);
                _currentBlock->setAngularVelocity(delta.x*frequency);
                _delta.y = delta.y * k_rotation;
                _keyRotate = true;
            }
            else
            {
                // 3 rotate();
                //                _currentBlock->setAngularVelocity(-delta.y * k_rotation);
                _delta.y = - delta.y * k_rotation;
                _keyRotate = true;
                
            }
        }
    }
    else if ( dx < dy2 )
    {
        if ( -dx > dy2 )
        {
            // 4 moveLeft;
            //            _currentBlock->translate( delta.x, 0.0);
            _currentBlock->setVelocity(delta.x*frequency, 0.0);
            //            _delta.x = dx;
            _keyTranslate = true;
        }
        else
        {
            if ( dx < 0 )
            {
                // 5 rotate();
                //                _currentBlock->rotate(-delta.y * k_rotation);
                _delta.y = -delta.y * k_rotation;
                _keyRotate = true;
            }
            else
            {
                // 6  rotate();
                //                _currentBlock->rotate(delta.y * k_rotation);
                _delta.y = delta.y * k_rotation;
                _keyRotate = true;
            }
        }
    }
}