//
//  GameLayer.h
//  game
//
//  Created by Federico Spadoni on 10/04/15.
//
//

#ifndef __game__GameLayer__
#define __game__GameLayer__


#include "cocos2d.h"


namespace cocos2d {
    class PhysicsScene;
    class SoftBodyMaterial;
    class SoftBodyDrawNode;
    
}

class Block;
class LevelLoader;


namespace CollisionCategory
{
    static unsigned int block = 1;
    static unsigned int currentBlock = 2;
    static unsigned int platform = 4;
}


class GameLayer : public cocos2d::Layer
{
    
    
    GameLayer(const LevelLoader* loader);
    
    virtual ~GameLayer();
    
public:
    
    static GameLayer* create(const LevelLoader* loader);
    
    //    static GameLayer *createWithPhysics(cocos2d::PhysicsWorld* physicsWorld);
    
    //    static cocos2d::Scene* createScene(const LevelLoader* loader);
    
    virtual bool init();
    
    virtual void onEnter() override;
    virtual void onExit() override;
    virtual void cleanup() override;
    
    virtual void update(float delta) override;
    
    void updateThreadSafe(float delta);
    
    
    //    void restartCallback(Ref* sender);
    //    void mainMenuCallback(Ref* sender);
    //    void toggleDebugCallback(Ref* sender);
    
    void toggleDebugCallback(bool debugDraw);
    
    
    //    typedef std::function<bool(PhysicsWorld&, PhysicsShape&, void*)> PhysicsQueryRectCallbackFunc;
    //    cocos2d::PhysicsQueryRectCallbackFunc levelCompleted;
    bool checkForLevelCompletion(cocos2d::PhysicsWorld& world, cocos2d::PhysicsShape& shape, void* data);
    bool levelCompleted(cocos2d::PhysicsWorld& world, cocos2d::PhysicsShape& shape, void* data);
    
    void increaseBlockFragmentCounter();
    void increaseBlockBrokenCounter();
    
private:
    
    void initBoard();
    
    bool initLevel();
    
    
    void addSoftBodyDrawNode(cocos2d::SoftBodyDrawNode* node, cocos2d::SoftBodyMaterial* mat)
    {_softbodyDrawNodes[mat] = node;}
    
    void pushNextBlock(float dt);
    
    
    //    void pushNextBlock(cocos2d::PhysicsSoftBody* block);
    
    virtual bool onTouchBegan(cocos2d::Touch* touch, cocos2d::Event* event);
    virtual void onTouchMoved(cocos2d::Touch* touch, cocos2d::Event* event);
    virtual void onTouchEnded(cocos2d::Touch* touch, cocos2d::Event* event);
    
    void onTouchMovedOld(cocos2d::Touch* touch, cocos2d::Event* event);
    void onTouchMovedOldOld(cocos2d::Touch* touch, cocos2d::Event* event);
    
    virtual void onKeyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
    virtual void onKeyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event);
    
    
//    threadSafe callbacks
    bool doTouchBegan();
    bool doTouchMoved();
    bool doTouchEnded();
    
    bool doPushNextBlock();
    bool doLevelCompleted();
    bool doIncreaseBlockFragmentCounter();
    bool doIncreaseBrokenCounter();
    bool doStackFailed();
    
    
    typedef std::function<bool()> ThreadSafeCallback;
    typedef std::vector< ThreadSafeCallback >::const_iterator ThreadSafeCallbacksIter;
    std::vector< ThreadSafeCallback > _threadSafeCallbacks;
    
    
    const LevelLoader* _loader;
    
    cocos2d::PhysicsScene* _scene;
    cocos2d::PhysicsWorld* _physicsWorld;
    //    bool _debugDraw;
    
    // to fast recover the draw node from the material
    typedef std::map< cocos2d::SoftBodyMaterial* ,cocos2d::SoftBodyDrawNode*> SoftBodyDrawNodes;
    typedef std::map< cocos2d::SoftBodyMaterial* ,cocos2d::SoftBodyDrawNode*>::iterator SoftBodyDrawNodesIterator;
    typedef std::map< cocos2d::SoftBodyMaterial* ,cocos2d::SoftBodyDrawNode*>::const_iterator SoftBodyDrawNodesConstIterator;
    
    SoftBodyDrawNodes _softbodyDrawNodes;
    
    Block* _currentBlock;
    cocos2d::Vector<Block*> _levelLoadedBlocks;
    

    cocos2d::Sprite* _brokenBlockSprites[4];
    
    cocos2d::Label* _scoreLabel;
    cocos2d::Sprite* _scoreIcon;
    
    int _blockFragmentCounter;
    int _blockBrokenCounter;
    
    //    keyboard control
    bool _keyTranslate, _keyRotate;

    cocos2d::Vec2 _delta;
    
    
};




#endif /* defined(__game__GameLayer__) */
