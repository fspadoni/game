//
//  LevelLoader.h
//  game
//
//  Created by Federico Spadoni on 24/02/15.
//
//

#ifndef __game__LevelLoader__
#define __game__LevelLoader__

#include "cocos2d.h"


NS_CC_BEGIN
class TriangularMesh;
class SoftBodyMaterial;
NS_CC_END


class LevelBlock : public cocos2d::Ref
{
public:
    
    LevelBlock();
    
    ~LevelBlock();
    
    void setName(const std::string& name) { _name = name; }
    
    void setMesh(cocos2d::TriangularMesh* mesh);
    void setMaterial(cocos2d::SoftBodyMaterial* material);
    
    void setPosX(const float x) { _position.x = x; }
    void setPosY(const float y) { _position.y = y; }
    
    const std::string& getName() { return _name; }
    
    cocos2d::TriangularMesh* getMesh() {return _triMesh;}
    cocos2d::SoftBodyMaterial* getMaterial() { return _material; }
    
    cocos2d::Vec2 getPosition() { return _position; }
    
private:
    
    std::string _name;
    cocos2d::TriangularMesh* _triMesh;
    cocos2d::SoftBodyMaterial* _material;
    cocos2d::Vec2 _position;
    
};


class LevelLoader : public cocos2d::Ref
{
    
    
    enum {
        _maxNumLevels = 100,
    };
    
    
public:
    
    
    
    static LevelLoader* load(const std::string& levelName);
    
    static const char* getFirstLevel();
    
    static const char* getNextLevel();
    
    static const char* getPreviousLevel();
    
    static bool searchLevels();
    
    const cocos2d::Vector<LevelBlock*>& getLevelBlocks() const { return _levelBlocks; }
    
private:
    
    LevelLoader(const std::string& levelName);
    
    virtual ~LevelLoader();
    
    bool init();
    
    const std::string& _levelFileName;
    
    std::string _name;
    
    cocos2d::Vector<LevelBlock*> _levelBlocks;
    
    static std::vector<std::string> levels;
    
    static int currentLevel;
};


#endif /* defined(__game__LevelLoader__) */
