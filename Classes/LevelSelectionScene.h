//
//  LevelSelectionScene.h
//  game
//
//  Created by Federico Spadoni on 23/02/15.
//
//

#ifndef __game__LevelSelectionScene__
#define __game__LevelSelectionScene__


#include "cocos2d.h"


namespace cocos2d {
    class MenuItem;
}



class LevelSelectionScene : public cocos2d::Layer
{
    
public:
    static cocos2d::Scene* create();
    
    
    bool loadLevels(const std::string& filename);
    
    
private:
    
    void selectLevel(cocos2d::Ref* sender);
    
    
    cocos2d::Vector<cocos2d::MenuItem*> _levels;
    
};

#endif /* defined(__game__LevelSelectionScene__) */
