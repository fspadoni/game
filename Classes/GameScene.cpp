//
//  GameScene.cpp
//  game
//
//  Created by Federico Spadoni on 05/11/14.
//
//

#include "GameScene.h"
#include "main.h"
#include "GameLayer.h"
#include "LevelLoader.h"
#include "GameConfig.h"
#include "lua_helper_funcs.h"
#include "VisibleRect.h"

#include "physics/CCTriangularMesh.h"

#include "physics-render/CCSoftBodyDrawNode.h"

USING_NS_CC;
//USING_NS_CC_EXT;




Scene* GameScene::create(LevelLoader* loader)
{
    
    if (loader == nullptr)
    {
        const char* levelName = LevelLoader::getFirstLevel() ;
        loader = LevelLoader::load( levelName );
    }
    
    GameScene *scene = new (std::nothrow) GameScene(loader);
    
    if (scene && scene->init() )
    {
        
        callLuaFunction<Scene>("scripts/main.lua","onCreateGameScene", scene, "cc.Scene");
        
        
        auto layer = GameLayer::create(loader);
        callLuaFunction<Layer>("scripts/main.lua","onCreateGameLayer", layer, "cc.Layer");
        
        layer->setTag(_gameLayerTag);
        
        //    // add layer as a child to scene
        scene->addChild(layer);
        
        scene->autorelease();
        return scene;
    }
    else
    {
        CC_SAFE_DELETE(scene);
        return nullptr;
    }
    
    // return the scene
    return scene;
}


GameScene::GameScene(LevelLoader* loader)
: PhysicsScene(16, 1000)
, _levelLoader(loader)
, _debugDraw(false)
//, _toggleDebug(false)
//, _reStart(false)
//, _mainMenu(false)
//, _nextLevel(false)
//, _previousLevel(false)
, buffer(nullptr)
{
    
}


GameScene::~GameScene()
{
    _levelLoader->release();
}


bool GameScene::init()
{
    
    if ( !PhysicsScene::initWithPhysics() )
        return false;
    
    
    _levelLoader->retain();
    
//    _physicsWorld->setAutoStep(false);
    _physicsWorld->setSubsteps(2);
    _physicsWorld->setGravity( GameConfig::getInstance()->_gravity );
    
    _physicsWorld->setCollisionBias(0.1);
    _physicsWorld->setCollisionSlop(0.125);
    _physicsWorld->setIterations(12);
    _physicsWorld->setSleepTimeThreshold(100.0/32.0);
    //    _physicsWorld->setIdleSpeedThreshold();
    
    return true;
}


void GameScene::onEnter()
{
    Node::onEnter();
    
    _threadSafeCallbacks.clear();
    
    _gameLayer = static_cast<GameLayer*>(getChildByTag(_gameLayerTag) );
    
    
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    MenuItemFont::setFontSize(24);
    auto startItem = MenuItemFont::create("reStart", CC_CALLBACK_1(GameScene::restartCallback, this));
    auto reStartMenu = Menu::create(startItem, nullptr);
    this->addChild(reStartMenu,1);
    reStartMenu->setPosition(Vec2(VisibleRect::left().x+startItem->getContentSize().width, VisibleRect::top().y-startItem->getContentSize().height));
    
    MenuItemFont::setFontSize(24);
    auto stopItem = MenuItemFont::create("stop", CC_CALLBACK_1(GameScene::mainMenuCallback, this));
    auto stopMenu = Menu::create(stopItem, nullptr);
    this->addChild(stopMenu,1);
    stopMenu->setPosition(Vec2(VisibleRect::right().x-startItem->getContentSize().width, VisibleRect::top().y-startItem->getContentSize().height));
    
    // menu for debug layer
    MenuItemFont::setFontSize(24);
    auto debugItem = MenuItemFont::create("debug", CC_CALLBACK_1(GameScene::toggleDebugCallback, this));
    auto debugMenu = Menu::create(debugItem, nullptr);
    this->addChild(debugMenu,1);
    debugMenu->setPosition(Vec2(VisibleRect::right().x-debugItem->getContentSize().width, VisibleRect::bottom().y+debugItem->getContentSize().height));
    
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    auto nextLevelItem = MenuItemImage::create("f1.png", "f2.png", CC_CALLBACK_1(GameScene::nextLevel, this) );
    auto prevLevelItem = MenuItemImage::create("b1.png", "b2.png", CC_CALLBACK_1(GameScene::previousLevel, this) );
    
    nextLevelItem->setScale( visibleSize.height / 16 / nextLevelItem->getContentSize().height);
    prevLevelItem->setScale( visibleSize.height / 16 / nextLevelItem->getContentSize().height);
    
    auto levelMenu = Menu::create(nextLevelItem, prevLevelItem, nullptr);
    
    levelMenu->setPosition( VisibleRect::bottom().x, VisibleRect::bottom().y +  nextLevelItem->getContentSize().height);
    nextLevelItem->setPosition( nextLevelItem->getContentSize().width * 1.2 , 0.0 ); //0.0 , VisibleRect::bottom().y+nextLevelItem->getContentSize().height/2);
    prevLevelItem->setPosition( -prevLevelItem->getContentSize().width * 1.2, 0.0 ); //VisibleRect::center().x + prevLevelItem->getContentSize().width*2, VisibleRect::bottom().y+prevLevelItem->getContentSize().height/2);
    
    this->addChild(levelMenu,1);
    
    
    // start ffmpeg telling it to expect raw rgba 720p-60hz frames
    // -i - tells it to read frames from stdin
//    const char* cmd = "/usr/local/Cellar/ffmpeg/2.4.2/bin/ffmpeg -r 30 -f rawvideo -pix_fmt rgba -s 640x900 -i -  -threads 0 -preset fast -y -pix_fmt yuv420p -crf 21 -vf vflip -r 30 output.mp4"; // 640x960
//
////    const char* getcd
////    chdir("/some/where/else");
//    
//    // open pipe to ffmpeg's stdin in binary write mode
//    ffmpeg = popen(cmd, "w");
//    
//    if ( ffmpeg )
//    {
//        const int width = Director::getInstance()->getOpenGLView()->getFrameSize().width;
//        const int height = Director::getInstance()->getOpenGLView()->getFrameSize().height;
//        
//        buffer = new GLubyte[width * height * 4];
//    }

}


void GameScene::onEnterTransitionDidFinish()
{
    Node::onEnterTransitionDidFinish();
    
}

//void GameScene::update(float delta)
//{
//    PhysicsScene::update(delta);
//    
//}


void GameScene::setLevel(LevelLoader* levelLoader)
{
    _levelLoader = levelLoader;
    _levelLoader->retain();
}


void GameScene::cleanup()
{
    
    Scene::cleanup();
    
//    delete buffer;
//    buffer = nullptr;
//    pclose(ffmpeg);
}


bool GameScene::loadConfig(const std::string& filename)
{
    
}


bool GameScene::loadLevels(const std::string& filename)
{
    
}




bool GameScene::doToggleDebug()
{
    _debugDraw = !_debugDraw;
    _physicsWorld->setDebugDrawMask(_debugDraw ? PhysicsWorld::DEBUGDRAW_ALL : PhysicsWorld::DEBUGDRAW_NONE);
    
    _gameLayer->toggleDebugCallback(_debugDraw);
    
    return false;
}


bool GameScene::doRestart()
{
//    Director::getInstance()->pause();
    this->simulationPause();
    
    this->resetPhysicsWorld();
    
    //    auto gameLayer = getChildByTag(_gameLayerTag);
    removeChildByTag(_gameLayerTag);
    
    
    auto layer = GameLayer::create(_levelLoader);
    callLuaFunction<Layer>("scripts/main.lua","onCreateGameLayer", layer, "cc.Layer");
    
    layer->setTag(_gameLayerTag);
    
    //    // add layer as a child to scene
    this->addChild(layer);
    
    _gameLayer = static_cast<GameLayer*>(getChildByTag(_gameLayerTag) );

    return true;
}


bool GameScene::doMainMenu()
{
//    Director::getInstance()->pause();
    this->simulationPause();
    
    // create a scene. it's an autorelease object
    auto scene = Main::createScene();
    
    if (scene)
    {
        Director::getInstance()->replaceScene(scene);
    }
    
    return true;
}


bool GameScene::doNextLevel()
{
//    Director::getInstance()->pause();
    this->simulationPause();
    
    this->resetPhysicsWorld();
    
    //    auto gameLayer = getChildByTag(_gameLayerTag);
    removeChildByTag(_gameLayerTag);
    
    
    const char* nextLevelName = LevelLoader::getNextLevel() ;
    _levelLoader->release();
    _levelLoader = LevelLoader::load( nextLevelName );
    _levelLoader->retain();
    
    auto layer = GameLayer::create(_levelLoader);
    callLuaFunction<Layer>("scripts/main.lua","onCreateGameLayer", layer, "cc.Layer");
    
    layer->setTag(_gameLayerTag);
    
    //    // add layer as a child to scene
    this->addChild(layer);
    
    _gameLayer = static_cast<GameLayer*>(getChildByTag(_gameLayerTag) );
    
    return true;
}


bool GameScene::doPreviousLevel()
{
//    Director::getInstance()->pause();
    this->simulationPause();
    
    this->resetPhysicsWorld();
    
    //    auto gameLayer = getChildByTag(_gameLayerTag);
    removeChildByTag(_gameLayerTag);
    
    
    const char* prevLevelName = LevelLoader::getPreviousLevel() ;
    _levelLoader->release();
    _levelLoader = LevelLoader::load( prevLevelName );
    _levelLoader->retain();
    
    auto layer = GameLayer::create(_levelLoader);
    callLuaFunction<Layer>("scripts/main.lua","onCreateGameLayer", layer, "cc.Layer");
    
    layer->setTag(_gameLayerTag);
    
    //    // add layer as a child to scene
    this->addChild(layer);
    
    _gameLayer = static_cast<GameLayer*>(getChildByTag(_gameLayerTag) );
    
    return true;
}


void GameScene::updateThreadSafe(float delta)
{
    ThreadSafeCallbacksIter lastIter = _threadSafeCallbacks.end();
    for (ThreadSafeCallbacksIter iter = _threadSafeCallbacks.begin(); iter != lastIter; ++iter)
    {
        if ( (*iter)() )
        {
            _threadSafeCallbacks.clear();
            return;
        }
    }
    
    _threadSafeCallbacks.clear();

    
    if ( !isSimulationPaused() )
    {
        _gameLayer->updateThreadSafe(delta);
    }
    
    
    // video capture
//    const int width = Director::getInstance()->getOpenGLView()->getFrameSize().width;
//    const int height = Director::getInstance()->getOpenGLView()->getFrameSize().height;
    
    
//    static int captureFrame = 1;
//    
//    if ( ffmpeg && captureFrame )
//    {
//        glPixelStorei(GL_PACK_ALIGNMENT, 1);
//        glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
//        fwrite(buffer, width * height * 4, 1, ffmpeg);
//        captureFrame = 0;
//    }
//    else
//    {
//        captureFrame = 1;
//    }
    
    
}



