//
//  lua_helper_funcs.h
//  game
//
//  Created by Federico Spadoni on 05/11/14.
//
//

#ifndef game_lua_helper_funcs_h
#define game_lua_helper_funcs_h


//#include "scripting/lua-bindings/manual/network/lua_cocos2dx_network_manual.h"
//#include "cocosdenshion/lua_cocos2dx_cocosdenshion_manual.h"

#include "scripting/lua-bindings/manual/LuaBasicConversions.h"
#include "scripting/lua-bindings/manual/CCLuaEngine.h"


static int lua_module_register(lua_State* L)
{
//    register_network_module(L);
//    register_cocosdenshion_module(L);
    return 1;
}

//template<class T>
//T* callLuaFunction(const char* luaFileName,const char* functionName, const char* type)
//{
//    lua_State*  ls = LuaEngine::getInstance()->getLuaStack()->getLuaState();
//
//    std::string filefullpath = FileUtils::getInstance()->fullPathForFilename(luaFileName);
//    const char* pfilefullpath = filefullpath.c_str();
//    int isOpen = luaL_dofile(ls, pfilefullpath);
//    if(isOpen!=0){
//        const char* errormsg = lua_tostring(ls, -1);
//        CCLOG("%s", errormsg);
//
//        CCLOG("Open Lua Error: %i", isOpen);
//        return NULL;
//    }
//
//    lua_getglobal(ls, functionName);
//
////    lua_pushlightuserdata(ls, mainLayer);
////    lua_setglobal(ls,"mainLayer");
//
//    T* scene = 0;
//
//    int err = lua_pcall(ls, 0, 1, 0);
//    if ( err )
//    {
//        const char* errormsg = lua_tostring(ls, -1);
//        CCLOG("%s", errormsg);
//    }
//    else
//    {
//
//        luaval_to_object<T>(ls, -1, type, &scene);
//    }
//
//    return scene;
//}

template<class T>
void callLuaFunction(const char* luaFileName,const char* functionName, T* object, const char* type)
{
    LuaStack* stack = LuaEngine::getInstance()->getLuaStack();
    lua_State*  ls = LuaEngine::getInstance()->getLuaStack()->getLuaState();

    
    FileUtils* utils = FileUtils::getInstance();

    ssize_t chunkSize = 0;
    std::string chunkName = utils->fullPathForFilename(luaFileName);
    unsigned char* chunk = nullptr;
    
    if (utils->isFileExist(chunkName))
    {
        chunk = utils->getFileData(chunkName.c_str(), "rb", &chunkSize);
    }
    
    if (chunk)
    {
        // copy the file contents and add '\0' at the end, or the lua parser can not parse it
        std::string luaCode((const char*)chunk, chunkSize + 1);
        luaCode.back() = '\0';
        
//	    char * luaCodes = new char[chunkSize + 1];
//	    luaCodes[chunkSize] = '\0';
//	    memcpy(luaCodes, chunk, chunkSize);
//        stack->luaLoadBuffer(ls, (char*)chunk, (int)chunkSize, chunkName.c_str());
//        int err = luaL_dostring(ls, luaCode.c_str());
        int err = luaL_loadstring(ls, luaCode.c_str());
        if ( err )
        {
            const char* errormsg = lua_tostring(ls, -1);
            CCLOG("%s", errormsg);
        }
        
        lua_pcall(ls, 0, LUA_MULTRET, 0);
        
//        delete luaCodes;
        delete []chunk;
    }
    else
    {
        CCLOG("can not get file data of %s", chunkName.c_str());
        return;
    }
    
    lua_getglobal(ls, functionName);       /* query function by name, stack: function */
    if (!lua_isfunction(ls, -1))
    {
        CCLOG("[LUA ERROR] name '%s' does not represent a Lua function", functionName);
        lua_pop(ls, 1);
        return;
    }
 
    tolua_pushusertype(ls, object, type);
    
    int err = lua_pcall(ls, 1, 0, 0);
    if ( err )
    {
        const char* errormsg = lua_tostring(ls, -1);
        CCLOG("%s", errormsg);
    }
    
}


#endif
