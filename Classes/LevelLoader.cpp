//
//  LevelLoader.cpp
//  game
//
//  Created by Federico Spadoni on 24/02/15.
//
//

#include "LevelLoader.h"

#include "GameConfig.h"

#include "platform/CCFileUtils.h"
#include "tinyxml2/tinyxml2.h"

#include "physics/CCMaterialProperty.h"
#include "physics/CCTriangularMesh.h"


USING_NS_CC;

const int kMaxStringLen = 128;

std::vector<std::string> LevelLoader::levels;
int LevelLoader::currentLevel = -1;

LevelBlock::LevelBlock()
: _triMesh(nullptr)
, _material(nullptr)
, _position(0.0,0.0)
{
    
}

LevelBlock::~LevelBlock()
{
    if (_triMesh)
        _triMesh->release();
    
    if (_material)
        _material->release();
}

void LevelBlock::setMesh(TriangularMesh* mesh)
{
    if (_triMesh)
    {
        _triMesh->release();
    }
    
    _triMesh = mesh;
    _triMesh->retain();
}

void LevelBlock::setMaterial(SoftBodyMaterial* material)
{
    if (_material)
    {
        _material->release();
    }
    
    _material = material;
    _material->retain();
}


 

static std::string initWithFormatAndValist(const char* format, va_list ap)
{
    char pBuf[64];
    if (pBuf != nullptr)
    {
        vsnprintf(pBuf, kMaxStringLen, format, ap);
        return  std::string(pBuf);
    }
    return std::string();
}

static std::string createWithFormat(const char* format, ...)
{
    char pBuf[kMaxStringLen];
    
    va_list ap;
    va_start(ap, format);
    vsnprintf(pBuf, kMaxStringLen, format, ap);
    va_end(ap);
    
    return  std::string(pBuf);
}


LevelLoader* LevelLoader::load(const std::string& levelName)
{
    LevelLoader* loader = new (std::nothrow) LevelLoader(levelName);
    
    if (loader && loader->init() )
    {
        loader->autorelease();
    }
    else{
        CC_SAFE_RELEASE(loader);
        return nullptr;
    }
    
    return loader;

}

const char* LevelLoader::getFirstLevel()
{
    if ( levels.empty() )
    {
        if ( !searchLevels() )
        {
            return nullptr;
        }
    }
    
    currentLevel = 0;
    return levels[0].c_str();
}


const char* LevelLoader::getNextLevel()
{
    if ( levels.empty() )
    {
        if ( !searchLevels() )
        {
            return nullptr;
        }
    }
    
    ++currentLevel;
    currentLevel = currentLevel % levels.size();
    
    return levels[currentLevel].c_str();
}


const char* LevelLoader::getPreviousLevel()
{
    if ( levels.empty() )
    {
        if ( !searchLevels() )
        {
            return nullptr;
        }
    }
    
    --currentLevel;
    
    if ( currentLevel < 0 )
        currentLevel = levels.size() - 1;
    
    return levels[currentLevel].c_str();
}


bool LevelLoader::searchLevels()
{
    int levelNumber = 0;
    int counter = _maxNumLevels;
    
    while ( counter-- )
    {
        
        std::string levelName = createWithFormat("levels/level%d.xml",levelNumber++);
        std::string fullpath = FileUtils::getInstance()->fullPathForFilename(levelName);
        
        
        if ( FileUtils::getInstance()->isFileExist(fullpath) )
        {
            LevelLoader::levels.push_back(fullpath);
        }

    }
    
    return LevelLoader::levels.empty() ? false : true;
}


LevelLoader::LevelLoader(const std::string& levelName)
: _levelFileName(levelName)
{
    
}


LevelLoader::~LevelLoader()
{
    
}


bool LevelLoader::init()
{

    // Read content from file
    // xml read
    ssize_t filesize;
    unsigned char *data = FileUtils::getInstance()->getFileData(_levelFileName, "rb", &filesize);
    std::string content((const char*)data,filesize);
    
    delete [] data;
    
    // xml parse
    tinyxml2::XMLDocument* document = new tinyxml2::XMLDocument();
    document->Parse(content.c_str());
    
    const tinyxml2::XMLElement* rootElement = document->RootElement();// Root
    
    if ( const tinyxml2::XMLAttribute* attribute = rootElement->FindAttribute("name") )
    {
        this->_name = attribute->Value();
        CCLOG("level name = %s", attribute->Value() );
    }
    
    const tinyxml2::XMLElement* element = rootElement->FirstChildElement();
    

    
    while (element)
    {
        
        if (strcmp("BLOCK", element->Name()) == 0)
        {
            
            LevelBlock* block = new LevelBlock();
            block->autorelease();
//            lblock->retain();
            
            // attributes
            const tinyxml2::XMLAttribute* attribute = element->FirstAttribute();
            
            while (attribute)
            {
                std::string name = attribute->Name();
                std::string value = attribute->Value();
                
                if ( name == "name" )
                {
                    block->setName(value);
                    CCLOG("block name = %s", element->Name());
                }
                
                else if ( name == "mesh" )
                {
                    if ( TriangularMesh* mesh = TriangularMesh::createFromFile(value, false) )
                    {
                        mesh->setScale(GameConfig::getInstance()->_levelBlockScale);
                        block->setMesh(mesh);
                    }
                    else
                    {
                        CCLOG("mesh %s not found", value.c_str() );
                    }
                }

                else if ( name == "material" )
                {
                    if ( SoftBodyMaterial* material = SoftBodyMaterial::createFromFile(value) )
                    {
                        block->setMaterial(material);
                    }
                    else
                    {
                        CCLOG("material %s not found", value.c_str() );
                    }
                }
                
                else if ( name == "x" )
                {
                    block->setPosX( atof(value.c_str()) );
                }
                
                else if ( name == "y" )
                {
                    block->setPosY( atof(value.c_str()) );
                }
            
                attribute = attribute->Next();
                
            }
            

            _levelBlocks.pushBack(block);
            
        }
        
        element = element->NextSiblingElement();
    }
    
    delete document;
    
    return true;
}

