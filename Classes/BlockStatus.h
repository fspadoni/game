//
//  BlockStatus.h
//  game
//
//  Created by Federico Spadoni on 05/06/15.
//
//

#ifndef __game__BlockStatus__
#define __game__BlockStatus__


class Block;


class BlockStatus
{
public:
    
	virtual ~BlockStatus() {}

    
	virtual void enter(Block*) = 0;
    
	virtual void execute(Block*) = 0;
    
	virtual void exit(Block*) = 0;
};


class StackedStatus : public BlockStatus
{
    
public:
 
    virtual ~StackedStatus() {}
    
	virtual void enter(Block*)
	{
	}
    
	virtual void execute(Block*)
	{
	}
    
	virtual void exit(Block*)
	{
	}
    
};

class ActiveStatus : public BlockStatus
{
    
public:
    
    virtual ~ActiveStatus() {}
    
	virtual void enter(Block*)
	{
	}
    
	virtual void execute(Block*)
	{
	}
    
	virtual void exit(Block*)
	{
	}
    
};

class FallingStatus : public BlockStatus
{
    
public:
 
    virtual ~FallingStatus() {}
    
	virtual void enter(Block*)
	{
	}
    
	virtual void execute(Block*)
	{
	}
    
	virtual void exit(Block*)
	{
	}
    
};



#endif /* defined(__game__BlockStatus__) */
