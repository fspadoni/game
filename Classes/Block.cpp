//
//  Block.cpp
//  game
//
//  Created by Federico Spadoni on 15/12/14.
//
//

#include "Block.h"
#include "GameConfig.h"
#include "GameLayer.h"
#include "physics/CCMaterialProperty.h"

#include "chipmunk.h"

USING_NS_CC;

ActiveStatus Block::Active;
StackedStatus Block::Stacked;
FallingStatus Block::Falling;


Block* Block::createFromTriangularMesh(TriangularMesh* mesh, SoftBodyMaterial* material)
{
    Block* block = new (std::nothrow) Block();
    
    if (block && block->init(mesh, material) )
    {
        block->autorelease();
    }
    else{
        CC_SAFE_RELEASE(block);
        return nullptr;
    }
    
    return block;
}


Block* Block::clone(const Block* other)
{
    Block* block = new (std::nothrow) Block();
    
    if (block && block->copy(other) )
    {
        block->autorelease();
    }
    else{
        CC_SAFE_RELEASE(block);
        return nullptr;
    }
    
    return block;
}


bool Block::init(cocos2d::TriangularMesh* mesh, cocos2d::SoftBodyMaterial* material)
{
    return PhysicsSoftBody::init(mesh, material);
}


Block::Block()
: _currentStatus(&Active)
, _contactCounter(0)
, _isStacked(false)
, _isLevelBlock(false)
{
//    setSleepCallback( std::bind(&Block::breakAllElement, this, std::placeholders::_1) );
}

Block::~Block()
{
    
}


void Block::sleepBegin(void)
{
    
}

void Block::breakElement(const unsigned short triangleIdx)
{
    _gameLayer->increaseBlockFragmentCounter();
}

void Block::breakAllElement()
{
    _isStacked = true;
    
    changeStatus(&Stacked);
    
    updateStatus();
    
    _gameLayer->increaseBlockBrokenCounter();
}



void Block::collisionBegin( cpBody* collidingBody, cpArbiter* arb )
{

    _isStacked = true;
    
//    setVelocity(GameConfig::getInstance()->_fallVelocity);
//    setGravity( getWorld()->getGravity() );
    
    if (const cpSoftBodyOpt* collidingSoftBody = cpBodyGetSoftBody(collidingBody) )
    {

        if ( Block* collidingBlock = static_cast<Block*>(cpSoftBodyOptGetUserData(collidingSoftBody)) )
        {
//        collision with a softbody
            if ( collidingBlock->isLevelBlock() && this->getStatus() != &Block::Stacked )
            {
//            collision with side walls
                changeStatus(&Falling);
                updateStatus();
            }
            else
            {
//              collision with other blocks
//                setVelocity(GameConfig::getInstance()->_fallVelocity);
                setGravity( getWorld()->getGravity() );
            
                changeStatus(&Stacked);
                updateStatus();
            }
        }
//        else
//        {
////           collision with other blocks
//            setVelocity(GameConfig::getInstance()->_fallVelocity);
//            setGravity( getWorld()->getGravity() );
//            
//            changeStatus(&Stacked);
//            updateStatus();
//        }
    }
    else
    {
//        collision with rigid floor
//        setVelocity(GameConfig::getInstance()->_fallVelocity);
        setGravity( getWorld()->getGravity() );
        
        changeStatus(&Stacked);
        updateStatus();
    }
    

    
    
    ++_contactCounter;
//    PhysicsSoftBody* collidingSoftBody =  static_cast<PhysicsSoftBody*>(cpSoftBodyOptGetUserData() );
}

void Block::collisionSeparate( cpBody* collidingBody, cpArbiter* arb )
{
    
    --_contactCounter;
}



void Block::changeStatus(BlockStatus* newStatus)
{
    _currentStatus->exit(this);
    
    _currentStatus = newStatus;
    
    _currentStatus->enter(this);
}

void Block::updateStatus()
{
    if ( _currentStatus )
    {
        _currentStatus->execute(this);
    }
}
