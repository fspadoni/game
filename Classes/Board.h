//
//  Board.h
//  game
//
//  Created by Federico Spadoni on 05/11/14.
//
//

#ifndef game_Board_h
#define game_Board_h

#include "cocos2d.h"

class Board : public cocos2d::Node
{
public:
    virtual bool init();

    CREATE_FUNC(Board);
    
    
};

#endif
