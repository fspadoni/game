//
//  main.h
//  game
//
//  Created by Federico Spadoni on 03/11/14.
//
//

#ifndef game_main_h
#define game_main_h

#include "cocos2d.h"



class LevelLoader;

class Main : public cocos2d::Layer
{
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();
    
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    
    virtual void onEnter() override;
    virtual void onExit() override;
    virtual void cleanup() override;
    
    
//    void postScriptInit(cocos2d::Layer* layer);
    
    void menuPlayCallback(cocos2d::Ref* pSender);
    
    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    
    // implement the "static create()" method manually
    CREATE_FUNC(Main);
    
private:
    
    const LevelLoader* loadFirstLevel();
    
};


#endif
