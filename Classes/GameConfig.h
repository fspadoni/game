//
//  GameConfig.h
//  game
//
//  Created by Federico Spadoni on 24/02/15.
//
//
#ifndef __game__GameConfig__
#define __game__GameConfig__

#include "cocos2d.h"



class GameConfig : public cocos2d::Ref
{
    
    
public:
    
    static GameConfig* getInstance();
    
    static void destroyInstance();
    
    cocos2d::Size _designSize;
    
    int _levelNumber;
    float _blockScale;
    float _levelBlockScale;
    
    cocos2d::Vec2 _fallVelocity;
    cocos2d::Vec2 _gravity;
    
    float _topGoalHeight;
    float _topGoalWidth;
    float _stackCheckGoalHeight;
    float _stackCheckGoalWidth;
    
//    game play
    int _maxBrokenBlocks;
    
    
CC_CONSTRUCTOR_ACCESS:
    GameConfig();
    
    virtual ~GameConfig();
    
private:
    
    static GameConfig* s_singleInstance;
    
};


#endif /* defined(__game__GameConfig__) */
