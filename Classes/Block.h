//
//  Block.h
//  game
//
//  Created by Federico Spadoni on 15/12/14.
//
//

#ifndef __game__Block__
#define __game__Block__

#include "cocos2d.h"
#include "physics/CCPhysicsSoftBody.h"

#include "BlockStatus.h"

NS_CC_BEGIN
class TriangularMesh;
class MaterialProperty;
NS_CC_END


class GameLayer;


class Block : public cocos2d::PhysicsSoftBody
{
public:
    
    //block status
    static StackedStatus Stacked;
	static ActiveStatus Active;
	static FallingStatus Falling;
    
    
    
    static Block* createFromTriangularMesh(cocos2d::TriangularMesh* mesh, cocos2d::SoftBodyMaterial* material);
    
    static Block* clone(const Block* other);
    
//    inline bool isOnBoard() { return getSoftBodyOpt();}
    inline bool isStacked() const { return _isStacked;}
    
    inline bool isInContact() const { return (_contactCounter > 0 ) ? true : false; }
    
    inline void setLayer(GameLayer* layer) { _gameLayer = layer; }
    
    void setLevelBlock(bool isLevelBlcok) {_isLevelBlock = isLevelBlcok; }
    
    BlockStatus* getStatus() { return _currentStatus; }
    
protected:
    

    Block();
    virtual ~Block();
    
    virtual bool init(cocos2d::TriangularMesh* mesh, cocos2d::SoftBodyMaterial* material);
    
    
    virtual void sleepBegin(void) override;
    
    virtual void breakElement(const unsigned short triangleIdx) override;
    
    virtual void breakAllElement() override;
    
    virtual void collisionBegin( cpBody* collidingBody, cpArbiter* arb ) override;
    
    virtual void collisionSeparate(cpBody* collidingBody, cpArbiter* arb) override;
    
    
//    status change
    void changeStatus(BlockStatus* newStatus);
    
	void updateStatus();
    
    
    bool isLevelBlock() const { return _isLevelBlock; }
    
    
private:

    
    GameLayer* _gameLayer;
    
    BlockStatus* _currentStatus;
    
    int _contactCounter;
    bool _isStacked;
    
    bool _isLevelBlock;
    
//    virtual bool init(void);
    
//    CREATE_FUNC(Block);
    
    
};


#endif /* defined(__game__Block__) */
