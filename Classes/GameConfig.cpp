//
//  GameConfig.cpp
//  game
//
//  Created by Federico Spadoni on 24/02/15.
//
//

#include "GameConfig.h"



USING_NS_CC;


GameConfig* GameConfig::s_singleInstance = nullptr;


GameConfig* GameConfig::getInstance()
{
    if (s_singleInstance == nullptr)
    {
        s_singleInstance = new (std::nothrow) GameConfig();
    }
    return s_singleInstance;
}


void GameConfig::destroyInstance()
{
    CC_SAFE_RELEASE_NULL(s_singleInstance);
}


GameConfig::GameConfig()
: _designSize( Size(320,480) )
, _blockScale(16.0)
, _levelBlockScale(32.0)
, _fallVelocity( cocos2d::Vec2(0.0, -260.0) )
, _gravity( cocos2d::Vect(0.0, -400) )
, _maxBrokenBlocks(3)
{
    _topGoalHeight = _designSize.height * 0.88;
    _topGoalWidth = _designSize.width * 0.33;
    _stackCheckGoalHeight = _designSize.height * 0.78;
    _stackCheckGoalWidth = _designSize.width * 0.33;
}


GameConfig::~GameConfig()
{
    
}