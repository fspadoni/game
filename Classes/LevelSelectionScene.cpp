//
//  LevelSelectionScene.cpp
//  game
//
//  Created by Federico Spadoni on 23/02/15.
//
//

#include "LevelSelectionScene.h"
#include "LevelLoader.h"
#include "GameConfig.h"
#include "GameScene.h"

#include "lua_helper_funcs.h"

USING_NS_CC;

const int kMaxStringLen = 128;


static std::string initWithFormatAndValist(const char* format, va_list ap)
{
    char pBuf[64];
    if (pBuf != nullptr)
    {
        vsnprintf(pBuf, kMaxStringLen, format, ap);
        return  std::string(pBuf);
    }
    return std::string();
}

static std::string createWithFormat(const char* format, ...)
{
    char pBuf[kMaxStringLen];
    
    va_list ap;
    va_start(ap, format);
    vsnprintf(pBuf, kMaxStringLen, format, ap);
    va_end(ap);
    
    return  std::string(pBuf);
}


Scene* LevelSelectionScene::create()
{
    //    read config file
    auto scene = Scene::create();
    
    callLuaFunction<Scene>("scripts/main.lua","onCreateLevelSelectionScene", scene, "cc.Scene");
    
    LevelSelectionScene *layer = new LevelSelectionScene();
    callLuaFunction<Layer>("scripts/main.lua","onCreateLevelSelectionLayer", layer, "cc.Layer");
    
    scene->addChild(layer);
    
    // return the scene
    return scene;
}



bool LevelSelectionScene::loadLevels(const std::string& filename)
{
    int levelNumber = 0;
    bool fileExists;
    
    // add menu items for tests
    TTFConfig ttfConfig("fonts/arial.ttf", 24);

    do {
        
//        FileUtils* utils = FileUtils::getInstance();
        std::string levelName = createWithFormat("level%d.xml",levelNumber);
        std::string fullpath = FileUtils::getInstance()->fullPathForFilename(levelName);
        
        if (fullpath.size() == 0)
        {
            fileExists = false;
        }
        
        if (fileExists)
        {
            
            MenuItemFont::setFontSize(24);
            MenuItem* item;
            
            std::string itemLabel = createWithFormat("Level %d",levelNumber);
            item = MenuItemFont::create(itemLabel.c_str(), CC_CALLBACK_1(LevelSelectionScene::selectLevel, this));
            
            //            auto label = Label::createWithTTF(ttfConfig, g_aTestNames[i].test_name);
            //            auto menuItem = MenuItemLabel::create(label, CC_CALLBACK_1(TestController::menuCallback, this));
            
            //                _itemMenu->addChild(menuItem, i + 10000);
            //                menuItem->setPosition(VisibleRect::center().x, (VisibleRect::top().y - (i + 1) * LINE_SPACE));
            
            //        else
            //        {
            //            item = CCMenuItemFont::create("New Level", this, menu_selector(LevelSelectionScene::selectLevel));
            //        }
            item->setTag(levelNumber);
            
            _levels.pushBack(item);
            
            levelNumber++;
        }
        
    } while (fileExists);
    
    Menu* menu = Menu::createWithArray(_levels);
    menu->alignItemsVertically();
    menu->setPosition(160,240);
    
    this->addChild(menu);
    
    return levelNumber ? true : false;
}



void LevelSelectionScene::selectLevel(Ref* sender)
{
    MenuItem* item = (MenuItem*) sender;
    int levelNumber = item->getTag();
    
    FileUtils* utils = FileUtils::getInstance();
    
    std::string levelName = createWithFormat("level%d.xml",levelNumber);
    std::string fullpath = FileUtils::getInstance()->fullPathForFilename(levelName);
    
    if (fullpath.size() == 0)
    {
        CCLOG("level %d not found",levelNumber);
        return;
    }
    
    CCLOG("loading level %d",levelNumber);
    
    LevelLoader *levelLoader = LevelLoader::load( fullpath );
    
    
    GameConfig::getInstance()->_levelNumber = levelNumber;
    
    // create the game scene and run it
    auto scene = GameScene::create(levelLoader);
//    auto scene = GameLayer::createScene(levelLoader);
    
    
    if (scene)
    {
        //        auto fade = TransitionFade::create(3, scene)
        Director::getInstance()->replaceScene(scene);
        //        scene->release();
    }
    
}