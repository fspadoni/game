//
//  BlockFactory.cpp
//  game
//
//  Created by Federico Spadoni on 08/11/14.
//
//

#include "BlockFactory.h"
#include "physics/CCTriangularMesh.h"
//#include "physics/CCPhysicsSoftBody.h"
#include "Block.h"

USING_NS_CC;


BlockFactory* BlockFactory::s_singleInstance = nullptr;

BlockFactory* BlockFactory::getInstance()
{
    if (s_singleInstance == nullptr)
    {
        s_singleInstance = new (std::nothrow) BlockFactory();
    }
    return s_singleInstance;
}

void BlockFactory::destroyInstance()
{
    CC_SAFE_RELEASE_NULL(s_singleInstance);
}


BlockFactory::BlockFactory()
: _scaleBlock(1.0)
{
    
//    memoryBuffer = Buffer<char>::create(STARTUP_MEMORY_SIZE_ALLOCATED);
    
//    PageMemoryPool::getInstance()->init_memory(memoryBuffer->lockForWrite(), memoryBuffer->getSizeInBytes());
   
}

BlockFactory::~BlockFactory()
{
//    PageMemoryPool::getInstance()->destroyInstance();
}

bool BlockFactory::addElementFromFile(const std::string &filename )
{
//    std::string fullpath = FileUtils::getInstance()->fullPathForFilename(filename);

    if ( TriangularMesh* tmesh = TriangularMesh::createFromFile(filename, false) )
    {
        tmesh->setScale(_scaleBlock);
        
        _triMeshes.pushBack(tmesh);
        
        
        if ( !_softBodiesMap.empty() )
        {
            _softBodiesMapIterator endIter = _softBodiesMap.end();
            for (_softBodiesMapIterator iter = _softBodiesMap.begin(); iter != endIter; iter++)
            {
                iter->second.pushBack( Block::createFromTriangularMesh(tmesh, iter->first) );
            }
            
        }
        
        
    }
    
    return true;
}

void BlockFactory::reset()
{
    _triMeshes.clear();
    _materials.clear();
    _softBodiesMap.clear();
}

bool BlockFactory::isInitialized()
{
    return !_softBodiesMap.empty();
}

bool BlockFactory::addMaterialFromFile(const std::string &filename)
{
    std::string fullpath = FileUtils::getInstance()->fullPathForFilename(filename);
    if (filename.size() == 0)
    {
        return false;
    }
    
    if ( auto material = SoftBodyMaterial::createFromFile(filename) )
    {
        _materials.pushBack(material);
        
        Vector<Block*> softbodies;
        
        Vector<TriangularMesh*>::const_iterator endIter = _triMeshes.end();
        for (Vector<TriangularMesh*>::const_iterator iter = _triMeshes.begin(); iter != endIter; iter++)
        {
            softbodies.pushBack(Block::createFromTriangularMesh(*iter, material) );
        }

        _softBodiesMap[material] = softbodies;
        
    }
    return true;
}


Block* BlockFactory::getBlock()
{
    
    if (!_softBodiesMap.empty())
    {
        ssize_t randIdx = rand() % _softBodiesMap.size();
        _softBodiesMapConstIterator randIter = _softBodiesMap.begin();
        std::advance(randIter , randIdx);
        const Vector<Block*>& softbodies = randIter->second;
        Block* softbody = softbodies.getRandomObject();
        
        Block* block = Block::clone(softbody);
        return block;
        
    }
    return nullptr;
}

